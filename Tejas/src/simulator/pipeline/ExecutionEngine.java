package pipeline;

import java.io.FileWriter;
import java.io.IOException;

import config.CoreConfig;
import config.PowerConfigNew;
import config.SystemConfig;
import config.BranchPredictorConfig.BP;
import pipeline.branchpredictor.AlwaysNotTaken;
import pipeline.branchpredictor.AlwaysTaken;
import pipeline.branchpredictor.BimodalPredictor;
import pipeline.branchpredictor.BranchPredictor;
import pipeline.branchpredictor.GAgpredictor;
import pipeline.branchpredictor.GApPredictor;
import pipeline.branchpredictor.GShare;
import pipeline.branchpredictor.NoPredictor;
import pipeline.branchpredictor.PAgPredictor;
import pipeline.branchpredictor.PApPredictor;
import pipeline.branchpredictor.PerfectPredictor;
import pipeline.branchpredictor.TournamentPredictor;
import generic.Core;
import generic.GenericCircularQueue;
import generic.Instruction;
import memorysystem.CoreMemorySystem;

public abstract class ExecutionEngine {
	
	protected Core containingCore;
	protected boolean executionComplete;
	protected CoreMemorySystem coreMemorySystem;

	private long instructionMemStall;
	
	private BranchPredictor branchPredictor;
	
	public ExecutionEngine(Core containingCore)
	{
		this.containingCore = containingCore;
		executionComplete = false;
		coreMemorySystem = null;
		instructionMemStall=0;
		
		CoreConfig coreConfig = SystemConfig.core[containingCore.getCore_number()];
		
		if(coreConfig.branchPredictor.predictorMode == BP.NoPredictor)
			this.branchPredictor = new NoPredictor(this);
		else if(coreConfig.branchPredictor.predictorMode == BP.PerfectPredictor)
			this.branchPredictor = new PerfectPredictor(this);
		else if(coreConfig.branchPredictor.predictorMode == BP.AlwaysTaken)
			this.branchPredictor = new AlwaysTaken(this);
		else if(coreConfig.branchPredictor.predictorMode == BP.AlwaysNotTaken)
			this.branchPredictor = new AlwaysNotTaken(this);
		else if(coreConfig.branchPredictor.predictorMode == BP.Tournament)
			this.branchPredictor = new TournamentPredictor(this);
		else if(coreConfig.branchPredictor.predictorMode == BP.Bimodal)
			this.branchPredictor = new BimodalPredictor(this, coreConfig.branchPredictor.PCBits,
					coreConfig.branchPredictor.saturating_bits);
		else if(coreConfig.branchPredictor.predictorMode == BP.GShare)
			this.branchPredictor = new GShare(this, coreConfig.branchPredictor.BHRsize, 
					coreConfig.branchPredictor.saturating_bits);
		else if(coreConfig.branchPredictor.predictorMode == BP.GAg)
			this.branchPredictor = new GAgpredictor(this, coreConfig.branchPredictor.BHRsize);
		else if(coreConfig.branchPredictor.predictorMode == BP.GAp)
			this.branchPredictor = new GApPredictor(this, coreConfig.branchPredictor.BHRsize, 
					coreConfig.branchPredictor.PCBits);
		else if(coreConfig.branchPredictor.predictorMode == BP.PAg)
			this.branchPredictor = new PAgPredictor(this, coreConfig.branchPredictor.PCBits, 
					coreConfig.branchPredictor.BHRsize, 
					coreConfig.branchPredictor.saturating_bits);
		else if(coreConfig.branchPredictor.predictorMode == BP.PAp)
			this.branchPredictor = new PApPredictor(this, coreConfig.branchPredictor.PCBits, 
					coreConfig.branchPredictor.BHRsize, 
					coreConfig.branchPredictor.saturating_bits);
	}
	
	public abstract void setInputToPipeline(GenericCircularQueue<Instruction>[] inpList);

	public void setExecutionComplete(boolean executionComplete) {
		this.executionComplete = executionComplete;
	}

	public boolean isExecutionComplete() {
		return executionComplete;
	}

	public void setCoreMemorySystem(CoreMemorySystem coreMemorySystem) {
		this.coreMemorySystem = coreMemorySystem;
	}

	public CoreMemorySystem getCoreMemorySystem() {
		return coreMemorySystem;
	}

	public void incrementInstructionMemStall(int i) {
		this.instructionMemStall += i;
		
	}

	public long getInstructionMemStall() {
		return instructionMemStall;
	}

	public Core getContainingCore() {
		return containingCore;
	}

	public BranchPredictor getBranchPredictor() {
		return branchPredictor;
	}
	
	public abstract PowerConfigNew calculateAndPrintPower(FileWriter outputFileWriter, String componentName) throws IOException;
	
}
