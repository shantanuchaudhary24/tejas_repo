package pipeline.amdk6;

import java.io.FileWriter;
import java.io.IOException;

import pipeline.ExecutionEngine;
import config.PowerConfigNew;
import config.SimulationConfig;
import generic.Core;
import generic.Event;
import generic.EventQueue;
import generic.GlobalClock;
import generic.Operand;
import generic.OperandType;
import generic.OperationType;
import generic.PortType;
import generic.SimulationElement;

public class WriteBackLogic_amd extends SimulationElement {
	
	Core core;
	OutOrderExecutionEngine_amd execEngine;
	ReorderBuffer_amd ROB;
	
	public WriteBackLogic_amd(Core core, OutOrderExecutionEngine_amd execEngine)
	{
		super(PortType.Unlimited, -1, -1, null, -1, -1);
		this.core = core;
		this.execEngine = execEngine;
		ROB = execEngine.getReorderBuffer();
	}

	@Override
	public void handleEvent(EventQueue eventQ, Event event) {
		
	}
	
	public void performWriteBack()
	{
		if(ROB.head == -1)
		{
			//ROB empty
			return;
		}
		
		if(execEngine.isToStall5() == true)
		{
			//pipeline stalled due to branch mis-prediction
			return;
		}
		
		int i = ROB.head;
		int noWritten = 0;
		
		ReorderBufferEntry_amd[] buffer = ROB.getROB();
		do
		{
			noWritten++;
			
			if(buffer[i].getExecuted() == true &&
					buffer[i].isWriteBackDone() == false)
			{
				buffer[i].setWriteBackDone1(true);
				buffer[i].setWriteBackDone2(true);
				
				/*
				 * aiding decoded instructions that are not yet in the IW.
				 * (see WakeUpLogic.java for detailed explanation)
				 * the below code is part of the solution, the remainder is at the wake-up stage
				 */
				if(buffer[i].getInstruction().getOperationType() == OperationType.load)
				{
					WakeUpLogic_amd.wakeUpLogic(core, buffer[i].getInstruction().getDestinationOperand().getOperandType(), buffer[i].getPhysicalDestinationRegister(), buffer[i].getThreadID(), (buffer[i].pos + 1)%ROB.MaxROBSize);
				}
				/*if(buffer[i].getInstruction().getDestinationOperand() != null && buffer[i].getInstruction().getDestinationOperand().getOperandType() == OperandType.machineSpecificRegister)
				{
					WakeUpLogic.wakeUpLogic(core, buffer[i].getInstruction().getDestinationOperand().getOperandType(), buffer[i].getPhysicalDestinationRegister(), buffer[i].getThreadID(), (buffer[i].pos + 1)%ROB.MaxROBSize);
				}
				if(buffer[i].getInstruction().getOperationType() == OperationType.xchg)
				{
					if(buffer[i].getInstruction().getSourceOperand1().getOperandType() == OperandType.machineSpecificRegister)
					{
						WakeUpLogic.wakeUpLogic(core, buffer[i].getInstruction().getSourceOperand1().getOperandType(), buffer[i].getOperand1PhyReg1(), buffer[i].getThreadID(), (buffer[i].pos + 1)%ROB.MaxROBSize);//(ROB.indexOf(buffer[i]) + 1) % ROB.MaxROBSize);
					}
					if(buffer[i].getInstruction().getSourceOperand2().getOperandType() == OperandType.machineSpecificRegister)
					{
						WakeUpLogic.wakeUpLogic(core, buffer[i].getInstruction().getSourceOperand2().getOperandType(), buffer[i].getOperand2PhyReg1(), buffer[i].getThreadID(), (buffer[i].pos + 1)%ROB.MaxROBSize);//(ROB.indexOf(buffer[i]) + 1) % ROB.MaxROBSize);
					}
				}*/
				
				
				//set value valid in register file, and
				//add destination register to list of available physical registers
				if(buffer[i].getInstruction().getDestinationOperand() != null)
				{
					writeToRFAndAddToAvailableList(buffer[i].getInstruction().getDestinationOperand(),
													buffer[i].getPhysicalDestinationRegister());
				}
				else if(buffer[i].getInstruction().getOperationType() == OperationType.xchg)
				{
					writeToRFAndAddToAvailableList(buffer[i].getInstruction().getSourceOperand1(),
													buffer[i].getOperand1PhyReg1());
					
					if(buffer[i].getInstruction().getSourceOperand1().getOperandType() != buffer[i].getInstruction().getSourceOperand2().getOperandType() ||
							buffer[i].getOperand1PhyReg1() != buffer[i].getOperand2PhyReg1())
					{
						writeToRFAndAddToAvailableList(buffer[i].getInstruction().getSourceOperand2(),
													buffer[i].getOperand2PhyReg1());
					}
				}

				if(SimulationConfig.debugMode)
				{
					System.out.println("writeback : " + GlobalClock.getCurrentTime()/core.getStepSize() + " : " + buffer[i].getInstruction());
				}
			}
			
			i = (i+1)%ROB.getMaxROBSize();
			
		}while(i != ROB.tail && noWritten < core.getRetireWidth());
	}
	
	//set value valid in register file, and
	//add destination register to list of available physical registers
	private void writeToRFAndAddToAvailableList(Operand destOpnd,
												int physicalRegister)
	{
		RenameTable_amd tempRN = null;
		if(destOpnd != null)
		{
			if(destOpnd.isIntegerRegisterOperand())
			{
				tempRN = execEngine.getIntegerRenameTable();
				if(tempRN.getMappingValid(physicalRegister) == false)
				{
					tempRN.addToAvailableList(physicalRegister);
				}
				tempRN.setValueValid(true, physicalRegister);
				execEngine.getIntegerRegisterFile().setValueValid(true, physicalRegister);
			}
			else if(destOpnd.isFloatRegisterOperand())
			{
				tempRN = execEngine.getFloatingPointRenameTable();
				if(tempRN.getMappingValid(physicalRegister) == false)
				{
					tempRN.addToAvailableList(physicalRegister);
				}
				tempRN.setValueValid(true, physicalRegister);
				execEngine.getFloatingPointRegisterFile().setValueValid(true, physicalRegister);
			}
		}
	}

}
