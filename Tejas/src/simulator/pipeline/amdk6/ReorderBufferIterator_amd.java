package pipeline.amdk6;

import java.util.Iterator;

public class ReorderBufferIterator_amd implements Iterator<ReorderBufferEntry_amd>{
	
		ReorderBuffer_amd ROB;
		boolean firstTime;
		int index;
		
		public ReorderBufferIterator_amd(ReorderBuffer_amd ROB, int pos) {
			
			this.ROB = ROB;
			this.index = pos;
			firstTime = true;
			
		}
		
		public boolean hasNext() {
		
			if(ROB.isIndexInside(index) && (!ROB.isFull() || (firstTime || index != ROB.head)))
				return true;
			return false;
		}

		public ReorderBufferEntry_amd next() {
			if(!hasNext())
				return null;
			ReorderBufferEntry_amd robEntry = ROB.getEntryAtIndex(index);
			index = (index + 1) % ROB.MaxROBSize;
			firstTime = false;
			return robEntry;
		}

		public void remove() {
			// not permitted..
			
		}

}
