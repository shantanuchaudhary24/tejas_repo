package pipeline.amdk6;

import config.SimulationConfig;
import generic.Core;
import generic.ExecCompleteEvent;
import generic.FunctionalUnitType;
import generic.GlobalClock;
import generic.Instruction;
import generic.OperationType;
import generic.RequestType;

/**
 * Code that represent an entry of Instruction Window
 */
public class IWEntry_amd {
	
	Core core;
	OutOrderExecutionEngine_amd execEngine;
	InstructionWindow_amd instructionWindow;
	
	Instruction instruction;
	ReorderBufferEntry_amd associatedROBEntry;
	OperationType opType;
	boolean isValid;
	boolean oneCyclePassed;				// For flagging whether one cycle has passed or not. This is important for the functioning of AMD k-6 when RAW hazard occurs.
	int pos;

	public IWEntry_amd(Core core, int pos,
			OutOrderExecutionEngine_amd execEngine, InstructionWindow_amd instructionWindow)
	{
		this.core = core;		
		this.execEngine = execEngine;
		this.instructionWindow = instructionWindow;
		this.pos = pos;
		isValid = false;
		oneCyclePassed = false;			// This is used in case of RAW hazard is found
	}
	
	// Issue stage changes
	public boolean issueInstruction()
	{
		if(!associatedROBEntry.isRenameDone())
			misc.Error.showErrorAndExit("cannot issue this instruction: Renaming not done!");
		else if(associatedROBEntry.getExecuted())
			misc.Error.showErrorAndExit("cannot issue this instruction: Already Executed!");

		// Here we check for operands. If the operands are available then we issue the corresponding instruction
		if(associatedROBEntry.isOperand1Available() && associatedROBEntry.isOperand2Available())
		{
			if(opType == OperationType.mov ||
					opType == OperationType.xchg)
			{
				issueMovXchg();
				return true;
			}
			
			else if(opType == OperationType.load ||
					opType == OperationType.store)
			{
				issueLoadStore();
				return true;
			}
			
			else 
			{
				return issueOthers();
			}
		}		

		return false;
	}
	
	void issueMovXchg()
	{
		//no FU required
		
		//associatedROBEntry.setIssued(true);
		
		//remove IW entry
		instructionWindow.removeFromWindow(this);
		
		//schedule completion of execution - 1 cycle operation
		core.getEventQueue().addEvent(
				new ExecCompleteEvent_amd(
						null,
						GlobalClock.getCurrentTime() + core.getStepSize(),
						null, 
						execEngine.getExecuter(),
						
						RequestType.EXEC_COMPLETE,
						associatedROBEntry));
		
		//dependent instructions can begin in the next cycle
		core.getEventQueue().addEvent(
				new BroadCastEvent_amd(
						GlobalClock.getCurrentTime(),
						null, 
						execEngine.getExecuter(),
						RequestType.BROADCAST,
						associatedROBEntry));
		

		if(SimulationConfig.debugMode)
		{
			System.out.println("issue : " + GlobalClock.getCurrentTime()/core.getStepSize() + " : "  + associatedROBEntry.getInstruction());
			System.out.println("Futype : " + OpTypeToFUTypeMapping_amd.getFUType(this.getInstruction().getOperationType()));	// To identify the Functional Unit for debugging.
		}		
	}
	
	void issueLoadStore()
	{
		//assertions
		if(associatedROBEntry.getLsqEntry().isValid() == true)
		{
			misc.Error.showErrorAndExit("Address valid.....Isuue Load/Store");
		}		
		if(associatedROBEntry.getLsqEntry().isForwarded() == true)
		{
			misc.Error.showErrorAndExit("Forwarded value is valid....Issue Load/Store");
		}
		
		//associatedROBEntry.setIssued(true);
		if(opType == OperationType.store)
		{
			//stores are issued at commit stage
			
			associatedROBEntry.setExecuted(true);
			associatedROBEntry.setWriteBackDone1(true);
			associatedROBEntry.setWriteBackDone2(true);
		}
		
		associatedROBEntry.setFUInstance(0);
		
		//remove IW entry
		instructionWindow.removeFromWindow(this);
		
		//tell LSQ that address is available
		execEngine.getCoreMemorySystem().issueRequestToLSQ(
				null, 
				associatedROBEntry);

		if(SimulationConfig.debugMode)
		{
			System.out.println("issue : " + GlobalClock.getCurrentTime()/core.getStepSize() + " : "  + associatedROBEntry.getInstruction());
			System.out.println("Futype : " + OpTypeToFUTypeMapping_amd.getFUType(this.getInstruction().getOperationType()));	// To identify the Functional Unit for debugging.
		}
	}
	
	boolean issueOthers()
	{
		FunctionalUnitType FUType = OpTypeToFUTypeMapping_amd.getFUType(opType);
		if(FUType == FunctionalUnitType.inValid)
		{
			associatedROBEntry.setIssued(true);
			associatedROBEntry.setFUInstance(0);
			
			//Invalid FU, remove entry from instruction window
			instructionWindow.removeFromWindow(this);
			return true;
		}
		
		long FURequest = 0;	//will be <= 0 if an FU was obtained
		//will be > 0 otherwise, indicating how long before
		//	an FU of the type will be available
		
		FURequest = execEngine.getFunctionalUnitSet().requestFU(FUType, GlobalClock.getCurrentTime(), core.getStepSize() );
		if(FURequest <= 0)
		{
			// Don't need to issue here. Issuing done at other places
			associatedROBEntry.setFUInstance((int) ((-1) * FURequest));
			
			//remove IW entry
			instructionWindow.removeFromWindow(this);
			
			core.getEventQueue().addEvent(
					new BroadCastEvent_amd(
							GlobalClock.getCurrentTime() + (core.getLatency(
									OpTypeToFUTypeMapping_amd.getFUType(opType).ordinal()) - 1) * core.getStepSize(),
							null, 
							execEngine.getExecuter(),
							RequestType.BROADCAST,
							associatedROBEntry));
			
			core.getEventQueue().addEvent(
					new ExecCompleteEvent_amd(
							null,
							GlobalClock.getCurrentTime() + core.getLatency(
									OpTypeToFUTypeMapping_amd.getFUType(opType).ordinal()) * core.getStepSize(),
							null, 
							execEngine.getExecuter(),
							RequestType.EXEC_COMPLETE,
							associatedROBEntry));

			if(SimulationConfig.debugMode)
			{
				System.out.println("issue : " + GlobalClock.getCurrentTime()/core.getStepSize() + " : "  + associatedROBEntry.getInstruction());
				System.out.println("Futype : " + OpTypeToFUTypeMapping_amd.getFUType(this.getInstruction().getOperationType()));	// To identify the Functional Unit for debugging.
			}
			
			return true;
		}
		else {					// FU is not available. remove instruction from window
			instructionWindow.removeFromWindow(this);
			associatedROBEntry.setIssued(false);
		}
		return false;
	}

	
	public ReorderBufferEntry_amd getAssociatedROBEntry() {
		return associatedROBEntry;
	}
	public void setAssociatedROBEntry(ReorderBufferEntry_amd associatedROBEntry) {
		this.associatedROBEntry = associatedROBEntry;
	}
	
	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public Instruction getInstruction() {
		return instruction;
	}

	public void setInstruction(Instruction instruction) {
		this.instruction = instruction;
		opType = instruction.getOperationType();
	}

}