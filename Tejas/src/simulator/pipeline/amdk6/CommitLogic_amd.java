package pipeline.amdk6;

import generic.Core;
import generic.Event;
import generic.EventQueue;
import generic.PortType;
import generic.SimulationElement;

public class CommitLogic_amd extends SimulationElement{

	private Core core;
	private OutOrderExecutionEngine_amd execEngine;
	private ReorderBuffer_amd ROB;
	private WriteBackLogic_amd writeBackLogic;

	public CommitLogic_amd(Core core, OutOrderExecutionEngine_amd execEngine)
	{
		super(PortType.Unlimited, -1, -1 ,null, -1, -1);
		this.core = core;
		this.execEngine = execEngine;
		ROB = execEngine.getReorderBuffer();
		//writeBackLogic= new WriteBackLogic_amd(core, execEngine);
	}
	
	
//TODO where to perform write back?
	public void performCommit() {
		ROB.performCommits();
	}

	@Override
	public void handleEvent(EventQueue eventQ, Event event) {
		// TODO Auto-generated method stub
		
	}

}
