package pipeline.amdk6;

import generic.Event;
import generic.RequestType;
import generic.SimulationElement;

public class MispredictionPenaltyCompleteEvent_amd extends Event {

	public MispredictionPenaltyCompleteEvent_amd(long eventTime,
			SimulationElement requestingElement,
			SimulationElement processingElement, RequestType requestType) {
		super(null, eventTime, requestingElement, processingElement, requestType, -1);
	}

}
