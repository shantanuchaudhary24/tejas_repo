package pipeline.amdk6;

import java.io.FileWriter;
import java.io.IOException;

import config.PowerConfigNew;
import config.SimulationConfig;
import generic.Core;
import generic.Event;
import generic.EventQueue;
import generic.GenericCircularQueue;
import generic.GlobalClock;
import generic.Instruction;
import generic.OperationType;
import generic.PortType;
import generic.SimulationElement;
/* This file contains the code for the decoder of the pipeline.
 * We have used this code as it is from the out of order pipeline
 * of Tejas. 
 * */
public class DecodeLogic_amd extends SimulationElement {
	
	Core core;
	OutOrderExecutionEngine_amd containingExecutionEngine;
	GenericCircularQueue<Instruction> fetchBuffer;
	GenericCircularQueue<ReorderBufferEntry_amd> decodeBuffer;
	int decodeWidth;
	long numAccesses;
	
	int invalidCount;
	
	public DecodeLogic_amd(Core core, OutOrderExecutionEngine_amd execEngine)
	{
		super(PortType.Unlimited, -1, -1 ,core.getEventQueue(), -1, -1);
		this.core = core;
		this.containingExecutionEngine = execEngine;
		fetchBuffer = execEngine.getFetchBuffer();
		decodeWidth = core.getDecodeWidth();
	}
	
	public void performDecode()
	{
		if(containingExecutionEngine.isToStall5() == true || containingExecutionEngine.isToStall6() == true)
		{
			//pipeline stalled due to branch mis-prediction
			return;
		}
		
		ReorderBuffer_amd ROB = containingExecutionEngine.getReorderBuffer();
		ReorderBufferEntry_amd newROBEntry;
		
		if(!containingExecutionEngine.isToStall1() &&
				!containingExecutionEngine.isToStall2())
		{
			for(int i = 0; i < decodeWidth; i++)
			{
				Instruction headInstruction = fetchBuffer.peek(0);
				if(headInstruction != null)
				{
					if(ROB.isFull())
					{
						containingExecutionEngine.setToStall4(true);
						break;
					}
					
					if(headInstruction.getOperationType() == OperationType.load ||
							headInstruction.getOperationType() == OperationType.store)
					{
						if(containingExecutionEngine.getCoreMemorySystem().getLsqueue().isFull())
						{
							containingExecutionEngine.setToStall3(true);
							break;
						}
					}
					
					newROBEntry = makeROBEntries(headInstruction);	// Insert head instruction into the buffer

					fetchBuffer.dequeue();
					
					incrementNumAccesses(1);
					
					if(SimulationConfig.debugMode)
					{
						System.out.println("decoded : " + GlobalClock.getCurrentTime()/core.getStepSize() + " : "  + headInstruction);
					}
				}
				
				containingExecutionEngine.setToStall3(false);
				containingExecutionEngine.setToStall4(false);
			}
		}
	}
	
	ReorderBufferEntry_amd makeROBEntries(Instruction newInstruction)
	{
		if(newInstruction != null)
		{
			ReorderBufferEntry_amd newROBEntry = containingExecutionEngine.getReorderBuffer()
											.addInstructionToROB(
													newInstruction,
													newInstruction.getThreadID());
			
			//if load or store, make entry in LSQ
			if(newInstruction.getOperationType() == OperationType.load ||
					newInstruction.getOperationType() == OperationType.store)
			{
				boolean isLoad;
				if (newInstruction.getOperationType() == OperationType.load)
					isLoad = true;
				else
					isLoad = false;
					
				containingExecutionEngine.getCoreMemorySystem().allocateLSQEntry(isLoad, 
						newROBEntry.getInstruction().getSourceOperand1MemValue(),
						newROBEntry);
			}
			
			return newROBEntry;
		}
		
		return null;
	}

	@Override
	public void handleEvent(EventQueue eventQ, Event event) {
		
	}
	
	void incrementNumAccesses(int incrementBy)
	{
		numAccesses += incrementBy;
	}

	public PowerConfigNew calculateAndPrintPower(FileWriter outputFileWriter, String componentName) throws IOException
	{
		PowerConfigNew power = new PowerConfigNew(containingExecutionEngine.getContainingCore().getDecodePower(), numAccesses);
		power.printPowerStats(outputFileWriter, componentName);
		return power;
	}

}
