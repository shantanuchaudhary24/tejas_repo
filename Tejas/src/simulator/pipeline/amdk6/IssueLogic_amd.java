package pipeline.amdk6;

import java.util.Iterator;

import config.SimulationConfig;
import generic.Core;
import generic.Event;
import generic.EventQueue;
import generic.FunctionalUnitType;
import generic.GenericCircularQueue;
import generic.GlobalClock;
import generic.OperationType;
import generic.PortType;
import generic.SimulationElement;
/* This class contains code for issuing instructions to the instruction window
 * from the scheduler buffer. This includes handling the type of functional
 * unit based upon the type of instruction. The number of instruction assignments
 * to a functional unit are fixed. This can be configured using the configuration
 * file. In our case this has been fixed (see constructor of class). The AMD K6
 * specifications say that the maximum number of instructions(operations) assigned
 * to a functional unit is one.
 * */
public class IssueLogic_amd extends SimulationElement{

	Core core;
	OutOrderExecutionEngine_amd containingExecutionEngine;
	GenericCircularQueue<ReorderBufferEntry_amd> decodeBuffer;
	ReorderBuffer_amd schedulerBuffer;					// Main scheduler for AMD pipeline
	InstructionWindow_amd IW;							// Instruction window for AMD pipeline
	
	int decodeWidth;			// Decode width from configuration file
	int issueWidth;				// Issue width from configuration file
	int schedulerBufferSize;	// Scheduler buffer size
	int IWSize;					// Instruction windows size
	
	int FUAsgnmnts[];			// To track the number of instruction assignments to a functional unit.
	int maxFU[];				// To store the maximum number of instruction assignments to a functional unit.
	
	public IssueLogic_amd(Core core, OutOrderExecutionEngine_amd execEngine) {
		
		super(PortType.Unlimited, -1, -1 ,core.getEventQueue(), -1, -1);
		this.core = core;
		this.containingExecutionEngine = execEngine;
		decodeBuffer = execEngine.getDecodeBuffer();
		decodeWidth = core.getDecodeWidth();
		schedulerBuffer = execEngine.getReorderBuffer();
		schedulerBufferSize = core.getReorderBufferSize();
		IW = execEngine.getInstructionWindow();
		IWSize = core.getIWSize();
		issueWidth = core.getIssueWidth();
		FUAsgnmnts= new int[FunctionalUnitType.no_of_types.ordinal()];
		maxFU = new int[FunctionalUnitType.no_of_types.ordinal()];
		
		// The maximum number of functional units are set here with the help from config file.
		for(int i = 0; i < FunctionalUnitType.no_of_types.ordinal(); i++) {
			FUAsgnmnts[i] = 0;		// Initially zero units are used
			if(i == FunctionalUnitType.integerALU.ordinal())
				maxFU[i] = execEngine.getFunctionalUnitSet().getNumberOfUnits(FunctionalUnitType.integerALU);
			else if(i == FunctionalUnitType.integerMul.ordinal())
				maxFU[i] = execEngine.getFunctionalUnitSet().getNumberOfUnits(FunctionalUnitType.integerMul);
			else if(i == FunctionalUnitType.integerDiv.ordinal())
				maxFU[i] = execEngine.getFunctionalUnitSet().getNumberOfUnits(FunctionalUnitType.integerDiv);
			else if(i == FunctionalUnitType.floatALU.ordinal())
				maxFU[i] = execEngine.getFunctionalUnitSet().getNumberOfUnits(FunctionalUnitType.floatALU);
			else if(i == FunctionalUnitType.floatMul.ordinal())
				maxFU[i] = execEngine.getFunctionalUnitSet().getNumberOfUnits(FunctionalUnitType.floatMul);
			else if(i == FunctionalUnitType.floatDiv.ordinal())
				maxFU[i] = execEngine.getFunctionalUnitSet().getNumberOfUnits(FunctionalUnitType.floatDiv);
			else maxFU[i] = 1;
		}
	}
	
	public void performIssue(){
	
		if(containingExecutionEngine.isToStall5() || containingExecutionEngine.isToStall1())
		{
			//pipeline stalled due to branch mis-prediction or full instruction window..
			return;
		}

		int totalIssued = 0;			// Count the number of entries that have been issued
		IWEntry_amd temp_IWEntry = null;
		assert(decodeBuffer.size() < decodeWidth + 1);	// For debugging
		ReorderBufferEntry_amd ROBEntry ;				// Entry from scheduler buffer is popped into this variable. 
		Iterator<ReorderBufferEntry_amd> iterator = schedulerBuffer.iterateOver();		// Iterator for running through the scheduler buffer
		while(iterator.hasNext())
		{
			if(totalIssued >= issueWidth) 					// we issue instructions as permitted by the issue width of the pipeline configuration
				break;
			ROBEntry = iterator.next();						// next scheduler buffer entry
			if(ROBEntry != null)
			{
				if(ROBEntry.getIssued())					// check if the entry from scheduler is already issued
					continue;
				if(ROBEntry.getInstruction().getOperationType() == OperationType.inValid ||
						ROBEntry.getInstruction().getOperationType() == OperationType.nop)
				{
					ROBEntry.setIssued(true);
					ROBEntry.setExecuted(true);
					ROBEntry.setWriteBackDone1(true);
					ROBEntry.setWriteBackDone2(true);
				}
				else
				{
					if(IW.isFull())
					{
						//containingExecutionEngine.setToStall1(true);
						// Don't need the above statement, if above statement is true, then Tejas will stall adding instructions to the instruction window
						break;
					}
					else
					{
						if(ROBEntry.getIssued())		// If the entry is already issued, then continue.
							continue;
						
						// Check functional unit type here, if this is not memory operation continue
						FunctionalUnitType FUType = OpTypeToFUTypeMapping_amd.getFUType(ROBEntry.getInstruction().getOperationType());
						if(FUType.ordinal() != FunctionalUnitType.memory.ordinal() && FUAsgnmnts[FUType.ordinal()] >= maxFU[FUType.ordinal()]) {		// if FU is available for use or not
							continue;
						}
						else {			// Increase the counter that counts the instructions assigned to the functional unit
							FUAsgnmnts[FUType.ordinal()]++;
						}
						
						if(SimulationConfig.debugMode)			// For debugging
							System.out.println("IW push : " + GlobalClock.getCurrentTime()/core.getStepSize() + " : "  + ROBEntry.getInstruction());
						
						// Add to Instruction Window....that is push back
						temp_IWEntry = IW.addToWindow(ROBEntry);
						if(temp_IWEntry == null)
							misc.Error.showErrorAndExit("Unable to find an empty entry in Instruction Window.");	
						
						// Set issued to true
						ROBEntry.setIssued(true);
						totalIssued++;			// increase the number of instructions issued
					}			
				}	
				containingExecutionEngine.setToStall1(false);	// Stall because scheduler buffer returned null. This means it doesn't contain any instructions.
			}
		}		
	}
	
	// if we have an unused functional unit then we use this function to indicate that the assignment to this functional unit has been withdrawn
	public void emptyFU(ReorderBufferEntry_amd reorderBufferEntry) {
			
			FunctionalUnitType fu = OpTypeToFUTypeMapping_amd.getFUType(reorderBufferEntry.getInstruction().getOperationType());
			FUAsgnmnts[fu.ordinal()]--;					// Decrease instruction assignment to the functional unit
			if(SimulationConfig.debugMode)				// For Debugging
				System.out.println("Futype decreased for : " + fu + " new value : " + FUAsgnmnts[fu.ordinal()]);
			
		}
	public void handleEvent(EventQueue eventQ, Event event) {
		
	}
}
