package pipeline.amdk6;

import java.io.FileWriter;
import java.io.IOException;

import config.CoreConfig;
import config.PowerConfigNew;
import memorysystem.CoreMemorySystem;
import pipeline.ExecutionEngine;
import generic.Core;
import generic.GenericCircularQueue;
import generic.Instruction;

/**
 * execution engine comprises of : decode logic, ROB, instruction window, register files,
 * rename tables and functional units
 */
public class OutOrderExecutionEngine_amd extends ExecutionEngine {

	//the containing core
	private Core core;
	
	//components of the execution engine
	private ICacheBuffer_amd iCacheBuffer;
	private FetchLogic_amd fetcher;
	private GenericCircularQueue<Instruction> fetchBuffer;
	private DecodeLogic_amd decoder;
	private GenericCircularQueue<ReorderBufferEntry_amd> decodeBuffer;
	private RenameLogic_amd renamer;
	private GenericCircularQueue<ReorderBufferEntry_amd> renameBuffer;
	private IWPushLogic_amd IWPusher;
	private SelectLogic_amd selector;
	private ExecutionLogic_amd executer;
	private WriteBackLogic_amd writeBackLogic;

	private ReorderBuffer_amd reorderBuffer;
	private InstructionWindow_amd instructionWindow;
	private RegisterFile_amd integerRegisterFile;
	private RegisterFile_amd floatingPointRegisterFile;
	private RenameTable_amd integerRenameTable;
	private RenameTable_amd floatingPointRenameTable;
	private FunctionalUnitSet_amd functionalUnitSet;
	private CommitLogic_amd commitModule;
	private OperandFetchLogic_amd operandfetchModule;
	private IssueLogic_amd issueModule;
	
	//Core-specific memory system (a set of LSQ, TLB and L1 cache)
	private OutOrderCoreMemorySystem_amd outOrderCoreMemorySystem;
	
	//flags defining type of stalls
	private boolean toStall1;					//if IW full
												//fetcher, decoder and renamer stall

	private boolean toStall2;					//if physical register cannot be
												//allocated to the dest of an instruction,
												//all subsequent processing must stall
												//fetcher and decoder stall
		
	private boolean toStall3;					//if LSQ full, and a load/store needs to be
												//allocated an entry
												//fetcher stall
	
	private boolean toStall4;					//if ROB full
												//fetcher stall
	
	private boolean toStall5;					//if branch mis-predicted
												//fetcher stall
	
	private boolean toStall6[];					//if physical register (msr) cannot be
												//allocated to the dest of an instruction,
												//all subsequent processing of that thread must stall
												//fetcher and decoder stall

	
	public long prevCycles;

	public OutOrderExecutionEngine_amd(Core containingCore)
	{
		super(containingCore);
		
		core = containingCore;
		
		
		reorderBuffer = new ReorderBuffer_amd(core, this);
		instructionWindow = new InstructionWindow_amd(core, this);
		integerRegisterFile = new RegisterFile_amd(core, core.getIntegerRegisterFileSize());
		integerRenameTable = new RenameTable_amd(this, core.getNIntegerArchitecturalRegisters(), core.getIntegerRegisterFileSize(), integerRegisterFile, core.getNo_of_input_pipes());
		floatingPointRegisterFile = new RegisterFile_amd(core, core.getFloatingPointRegisterFileSize());
		floatingPointRenameTable = new RenameTable_amd(this, core.getNFloatingPointArchitecturalRegisters(), core.getFloatingPointRegisterFileSize(), floatingPointRegisterFile, core.getNo_of_input_pipes());
				
		functionalUnitSet = new FunctionalUnitSet_amd(core, core.getAllNUnits(),
													core.getAllLatencies());
		
		// Initializing the different modules
		fetchBuffer = new GenericCircularQueue<Instruction>(Instruction.class, core.getDecodeWidth());
		fetcher = new FetchLogic_amd(core, this);
		decodeBuffer = new GenericCircularQueue<ReorderBufferEntry_amd>(ReorderBufferEntry_amd.class, core.getDecodeWidth());
		decoder = new DecodeLogic_amd(core, this);
		renameBuffer = new GenericCircularQueue<ReorderBufferEntry_amd>(ReorderBufferEntry_amd.class, core.getDecodeWidth());
		executer = new ExecutionLogic_amd(core, this);
		
		commitModule = new CommitLogic_amd(core, this);			// Initialization of commit module
		issueModule = new IssueLogic_amd(core, this);			// Initialization of issuer
		operandfetchModule = new OperandFetchLogic_amd(core, this);// Initialization of operand fetcher
		
		toStall1 = false;
		toStall2 = false;
		toStall3 = false;
		toStall4 = false;
		toStall5 = false;
		toStall6 = new boolean[1];
		for(int i = 0; i < 1; i++)
		{
			toStall6[i] = false;
		}
		prevCycles=0;
	}
	
	public CommitLogic_amd getCommitModule(){
		return commitModule;
	}
	
	public IssueLogic_amd getIssueModule(){
		return issueModule;
	}
	
	public OperandFetchLogic_amd getOperandFetchModule(){
		return operandfetchModule;
	}
	
	public ICacheBuffer_amd getiCacheBuffer() {
		return iCacheBuffer;
	}

	public Core getCore() {
		return core;
	}
	
	public DecodeLogic_amd getDecoder() {
		return decoder;
	}

	public RegisterFile_amd getFloatingPointRegisterFile() {
		return floatingPointRegisterFile;
	}

	public RenameTable_amd getFloatingPointRenameTable() {
		return floatingPointRenameTable;
	}

	public FunctionalUnitSet_amd getFunctionalUnitSet() {
		return functionalUnitSet;
	}

	public RegisterFile_amd getIntegerRegisterFile() {
		return integerRegisterFile;
	}

	public RenameTable_amd getIntegerRenameTable() {
		return integerRenameTable;
	}
	
	public ReorderBuffer_amd getReorderBuffer() {
		return reorderBuffer;
	}

	public InstructionWindow_amd getInstructionWindow() {
		return instructionWindow;
	}

	public void setInstructionWindow(InstructionWindow_amd instructionWindow) {
		this.instructionWindow = instructionWindow;
	}
	
	public boolean isToStall1() {
		return toStall1;
	}

	public void setToStall1(boolean toStall1) {
		this.toStall1 = toStall1;
	}

	public boolean isToStall2() {
		return toStall2;
	}

	public void setToStall2(boolean toStall2) {
		this.toStall2 = toStall2;
	}

	public boolean isToStall3() {
		return toStall3;
	}

	public void setToStall3(boolean toStall3) {
		this.toStall3 = toStall3;
	}

	public boolean isToStall4() {
		return toStall4;
	}

	public void setToStall4(boolean toStall4) {
		this.toStall4 = toStall4;
	}

	public boolean isToStall5() {
		return toStall5;
	}

	public void setToStall5(boolean toStall5) {
		this.toStall5 = toStall5;
	}

	public boolean isToStall6() {
		return toStall6[0];
	}

	public void setToStall6(boolean toStall5) {
		this.toStall6[0] = toStall5;
	}
	
	public GenericCircularQueue<Instruction> getFetchBuffer() {
		return fetchBuffer;
	}

	public GenericCircularQueue<ReorderBufferEntry_amd> getDecodeBuffer() {
		return decodeBuffer;
	}

	public GenericCircularQueue<ReorderBufferEntry_amd> getRenameBuffer() {
		return renameBuffer;
	}
	
	public FetchLogic_amd getFetcher() {
		return fetcher;
	}

	public RenameLogic_amd getRenamer() {
		return renamer;
	}

	public IWPushLogic_amd getIWPusher() {
		return IWPusher;
	}

	public SelectLogic_amd getSelector() {
		return selector;
	}

	public ExecutionLogic_amd getExecuter() {
		return executer;
	}

	public WriteBackLogic_amd getWriteBackLogic() {
		return writeBackLogic;
	}

	@Override
	public void setInputToPipeline(GenericCircularQueue<Instruction>[] inpList) {
		
		fetcher.setInputToPipeline(inpList);
		
	}
	
	public OutOrderCoreMemorySystem_amd getCoreMemorySystem()
	{
		return outOrderCoreMemorySystem;
	}

	public void setCoreMemorySystem(CoreMemorySystem coreMemorySystem) {
		this.coreMemorySystem = coreMemorySystem;
		this.outOrderCoreMemorySystem = (OutOrderCoreMemorySystem_amd)coreMemorySystem;
		this.iCacheBuffer = new ICacheBuffer_amd((int)(core.getDecodeWidth() *
											coreMemorySystem.getiCache().getLatency()));
		this.fetcher.setICacheBuffer(iCacheBuffer);
	}
	
	public PowerConfigNew calculateAndPrintPower(FileWriter outputFileWriter, String componentName) throws IOException
	{
		PowerConfigNew totalPower = new PowerConfigNew(0, 0);
		
		PowerConfigNew bPredPower =  getBranchPredictor().calculateAndPrintPower(outputFileWriter, componentName + ".bPred");
		totalPower.add(totalPower, bPredPower);
		
		PowerConfigNew decodePower =  getDecoder().calculateAndPrintPower(outputFileWriter, componentName + ".decode");
		totalPower.add(totalPower, decodePower);
		
		//PowerConfigNew renamePower =  getRenamer().calculateAndPrintPower(outputFileWriter, componentName + ".rename");
		//totalPower.add(totalPower, renamePower);
		
		PowerConfigNew lsqPower =  getCoreMemorySystem().getLsqueue().calculateAndPrintPower(outputFileWriter, componentName + ".LSQ");
		totalPower.add(totalPower, lsqPower);
		
		PowerConfigNew intRegFilePower =  getIntegerRegisterFile().calculateAndPrintPower(outputFileWriter, componentName + ".intRegFile");
		totalPower.add(totalPower, intRegFilePower);
		
		PowerConfigNew floatRegFilePower =  getFloatingPointRegisterFile().calculateAndPrintPower(outputFileWriter, componentName + ".floatRegFile");
		totalPower.add(totalPower, floatRegFilePower);
		
		PowerConfigNew iwPower =  getInstructionWindow().calculateAndPrintPower(outputFileWriter, componentName + ".InstrWindow");
		totalPower.add(totalPower, iwPower);
		
		PowerConfigNew robPower =  getReorderBuffer().calculateAndPrintPower(outputFileWriter, componentName + ".ROB");
		totalPower.add(totalPower, robPower);
		
		PowerConfigNew fuPower =  getFunctionalUnitSet().calculateAndPrintPower(outputFileWriter, componentName + ".FuncUnit");
		totalPower.add(totalPower, fuPower);
		
		PowerConfigNew resultsBroadcastBusPower =  getExecuter().calculateAndPrintPower(outputFileWriter, componentName + ".resultsBroadcastBus");
		totalPower.add(totalPower, resultsBroadcastBusPower);
		
		totalPower.printPowerStats(outputFileWriter, componentName + ".total");
		
		return totalPower;
	}
}