package pipeline.amdk6;

import java.io.FileWriter;
import java.io.IOException;

import config.PowerConfigNew;
import config.SimulationConfig;
import generic.Core;
import generic.Event;
import generic.EventQueue;
import generic.ExecCompleteEvent;
import generic.GlobalClock;
import generic.Operand;
import generic.OperationType;
import generic.PortType;
import generic.RequestType;
import generic.SimulationElement;
/* This class contains code for instructions when execution
 * is over and write back stage needs to be dealt with. That is
 * the scheme of commits in the AMD pipeline.
 * */
public class ExecutionLogic_amd extends SimulationElement {
	
	Core core;
	OutOrderExecutionEngine_amd execEngine;
	ReorderBuffer_amd ROB;
	
	long numResultsBroadCastBusAccess;
	
	public ExecutionLogic_amd(Core core, OutOrderExecutionEngine_amd execEngine)
	{
		super(PortType.Unlimited, -1, -1, core.getEventQueue(), -1, -1);
		
		this.core = core;
		this.execEngine = execEngine;
	}

	@Override
	public void handleEvent(EventQueue eventQ, Event event) {
				
		ROB = execEngine.getReorderBuffer();
		ReorderBufferEntry_amd reorderBufferEntry = null;
		
		if(event.getRequestType() == RequestType.EXEC_COMPLETE)
		{
			reorderBufferEntry = ((ExecCompleteEvent_amd)event).getROBEntry();
		}
		else if(event.getRequestType() == RequestType.BROADCAST)
		{
			reorderBufferEntry = ((BroadCastEvent_amd)event).getROBEntry();
		}
		else
		{
			misc.Error.showErrorAndExit("execution logic received unknown event " + event);
		}
				
		if(event.getRequestType() == RequestType.EXEC_COMPLETE)
		{
			handleExecutionCompletion(reorderBufferEntry);
		}
		else if(event.getRequestType() == RequestType.BROADCAST)
		{
			performBroadCast(reorderBufferEntry);
		}
	}
	
	public void handleExecutionCompletion(ReorderBufferEntry_amd reorderBufferEntry)
	{
		//assertions
		if(reorderBufferEntry.getExecuted() == true ||
				reorderBufferEntry.isRenameDone() == false ||
				reorderBufferEntry.getIssued() == false)
		{
			misc.Error.showErrorAndExit("Invalid; Execution of instruction could not be completed");
		}		
		if(reorderBufferEntry.getIssued() == false)
		{
			misc.Error.showErrorAndExit("Invalid: Execution complete but not issued");
		}
		if(reorderBufferEntry.getInstruction().getOperationType() == OperationType.load)
		{
			if(reorderBufferEntry.getLsqEntry().isValid() == false)
			{
				misc.Error.showErrorAndExit("Invalid: Load should not have completed execution");
			}
			if(reorderBufferEntry.getLsqEntry().isForwarded() == false)
			{
				misc.Error.showErrorAndExit("Invalid: Unforwarded load has completed execution");
			}
		}
		
		/* If the above assertions are passed successfully that means that the instruction has been executed successfully
		 * Mark that instruction as executed.
		 */
		
		reorderBufferEntry.setExecuted(true);
		
		//explicit wakeup for some instructions such as xchg
		if(reorderBufferEntry.getInstruction().getDestinationOperand() != null
				|| reorderBufferEntry.getInstruction().getOperationType() == OperationType.xchg)
		{
			performBroadCast(reorderBufferEntry);
			incrementResultsBroadcastBusAccesses(1);
			writeBack(reorderBufferEntry);			// Write back for xchg operation completion
		}
		else
		{
			/* For instructions that don't need explicit wake up calls.
			 * For instructions such as store, branch, jump, NOP which don't write
			 * to the register file.
			 * */
			reorderBufferEntry.setWriteBackDone1(true);
			reorderBufferEntry.setWriteBackDone2(true);
		}
		
		ROB.incrementNumAccesses(1);
		execEngine.getIssueModule().emptyFU(reorderBufferEntry);
		if(SimulationConfig.debugMode)
		{
			System.out.println("executed : " + GlobalClock.getCurrentTime()/core.getStepSize() + " : "  + reorderBufferEntry.getInstruction());
		}
		
	}
	
	//TODO If there is any dependent instruction in the Instruction Window, wake it up
	void performBroadCast(ReorderBufferEntry_amd reorderBufferEntry)
	{
		if(reorderBufferEntry.getInstruction().getDestinationOperand() != null)					// For other type of instructions
		{
			WakeUpLogic_amd.wakeUpLogic(core,
									reorderBufferEntry.getInstruction().getDestinationOperand().getOperandType(),
									reorderBufferEntry.getPhysicalDestinationRegister(),
									reorderBufferEntry.getThreadID(),
									(reorderBufferEntry.pos + 1)%ROB.MaxROBSize);
		}
		else if(reorderBufferEntry.getInstruction().getOperationType() == OperationType.xchg)		// For xchange instruction
		{
			WakeUpLogic_amd.wakeUpLogic(core,
									reorderBufferEntry.getInstruction().getSourceOperand1().getOperandType(),
									reorderBufferEntry.getOperand1PhyReg1(),
									reorderBufferEntry.getThreadID(),
									(reorderBufferEntry.pos + 1)%ROB.MaxROBSize);
			
			WakeUpLogic_amd.wakeUpLogic(core,
									reorderBufferEntry.getInstruction().getSourceOperand2().getOperandType(),
									reorderBufferEntry.getOperand2PhyReg1(),
									reorderBufferEntry.getThreadID(),
									(reorderBufferEntry.pos + 1)%ROB.MaxROBSize);
		}
	}
	
	void incrementResultsBroadcastBusAccesses(int incrementBy)
	{
		numResultsBroadCastBusAccess += incrementBy;
	}

	// Power statistics for executer
	public PowerConfigNew calculateAndPrintPower(FileWriter outputFileWriter, String componentName) throws IOException
	{
		PowerConfigNew power = new PowerConfigNew(core.getResultsBroadcastBusPower(), numResultsBroadCastBusAccess);
		power.printPowerStats(outputFileWriter, componentName);
		return power;
	}
	
	/* Code for writeback stage. This basically checks and frees registers for further use.
	 * */
	private void writeBack(ReorderBufferEntry_amd first) {
		
		first.setWriteBackDone1(true);
		first.setWriteBackDone2(true);
		if(first.getInstruction().getOperationType() == OperationType.load)			// Wake up for LOAD instruction
		{
			WakeUpLogic_amd.wakeUpLogic(core, first.getInstruction().getDestinationOperand().getOperandType(), first.getPhysicalDestinationRegister(), first.getThreadID(), (first.pos + 1)%ROB.MaxROBSize);
		}
		
		/* Set the value as valid in the register file and add the
		 * destination register to the list of available physical registers.
		 * */
		if(first.getInstruction().getDestinationOperand() != null)			// For other instructions
		{
			writeToRFAndAddToAvailableList(first.getInstruction().getDestinationOperand(),
											first.getPhysicalDestinationRegister());
		}
		else if(first.getInstruction().getOperationType() == OperationType.xchg)	// For xchange instruction
		{
			writeToRFAndAddToAvailableList(first.getInstruction().getSourceOperand1(),
											first.getOperand1PhyReg1());
			
			if(first.getInstruction().getSourceOperand1().getOperandType() != first.getInstruction().getSourceOperand2().getOperandType() ||
					first.getOperand1PhyReg1() != first.getOperand2PhyReg1())
			{
				writeToRFAndAddToAvailableList(first.getInstruction().getSourceOperand2(),
											first.getOperand2PhyReg1());
			}
		}
		
		// For Debug
		if(SimulationConfig.debugMode)
		{
			System.out.println("writeback : " + GlobalClock.getCurrentTime()/core.getStepSize() + " : " + first.getInstruction());
		}
		
	}
	
	/* Set the value as valid in the register file and add the
	 * destination register to the list of available physical registers.
	 * */
	private void writeToRFAndAddToAvailableList(Operand destOpnd,
												int physicalRegister)
	{
		RenameTable_amd tempRN = null;		// for storing rename Table reference
		if(destOpnd != null)
		{
			if(destOpnd.isIntegerRegisterOperand())					// For integer operand registers
			{
				tempRN = execEngine.getIntegerRenameTable();		// Take renaming table (integer) from the current execution engine
				if(!tempRN.getMappingValid(physicalRegister))		// If renaming mapping is valid, add the physical register to list of available registers
					tempRN.addToAvailableList(physicalRegister);
				
				tempRN.setValueValid(true, physicalRegister);		// set value valid for the register
				execEngine.getIntegerRegisterFile().setValueValid(true, physicalRegister);
			}
			else if(destOpnd.isFloatRegisterOperand())				// For floating point operand registers
			{
				tempRN = execEngine.getFloatingPointRenameTable();	// Take renaming table (float) from the current execution engine
				if(!tempRN.getMappingValid(physicalRegister))		// If renaming mapping is valid, add the physical register to list of available registers
					tempRN.addToAvailableList(physicalRegister);
				
				tempRN.setValueValid(true, physicalRegister);		// set value valid for the register
				execEngine.getFloatingPointRegisterFile().setValueValid(true, physicalRegister);
			}
		}
	}
}
