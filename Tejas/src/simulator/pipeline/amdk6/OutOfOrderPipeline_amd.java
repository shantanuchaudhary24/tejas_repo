package pipeline.amdk6;

import config.SimulationConfig;
import generic.Core;
import generic.EventQueue;
import generic.GenericCircularQueue;
import generic.GlobalClock;
import generic.Instruction;

public class OutOfOrderPipeline_amd implements pipeline.PipelineInterface {
	
	Core core;
	EventQueue eventQ;
	int coreStepSize;
	
	public OutOfOrderPipeline_amd(Core core, EventQueue eventQ)
	{
		this.core = core;
		this.eventQ = eventQ;
	}

	//New pipeline code interface is here
	public void oneCycleOperation() {
		
		SimulationConfig.debugMode = false;
//		SimulationConfig.debugMode = true;
		
		coreStepSize = core.getStepSize();			// No of cycles = Time/CycleTime
		if(coreStepSize == 0)
		{
			//this core has been initialised, but has no pipeline running on it
			return;
		}
		
		OutOrderExecutionEngine_amd execEngine;
		
		execEngine = (OutOrderExecutionEngine_amd) core.getExecEngine();
		
		long currentTime = GlobalClock.getCurrentTime();
		
		if(currentTime % coreStepSize == 0 && execEngine.isExecutionComplete() == false)
		{
			execEngine.getCommitModule().performCommit();
		}
		
		//handle events or in other words this is the execution stage of the simulated pipeline
		eventQ.processEvents();
	
		if(currentTime % coreStepSize == 0 && execEngine.isExecutionComplete() == false)
		{
			execEngine.getOperandFetchModule().performOperandFetch();		// Operand fetching module
			execEngine.getIssueModule().performIssue();						// Our Programmed issuer for issuing instructions to the K6 pipeline.
			execEngine.getDecoder().performDecode();						// Normal Decode Operation*
			execEngine.getFetcher().performFetch();							// Normal Fetch Operation
		}
		
	}
	public Core getCore() {
		return core;
	}

	public void setCore(Core core) {
		this.core = core;
	}

	public EventQueue getEventQ() {
		return eventQ;
	}

	public void setEventQ(EventQueue eventQ) {
		this.eventQ = eventQ;
	}

	@Override
	public boolean isExecutionComplete() {		
		return core.getExecEngine().isExecutionComplete();		
	}
	
	public void setcoreStepSize(int stepSize)
	{
		this.coreStepSize = stepSize;
	}
	
	public int getCoreStepSize()
	{
		return coreStepSize;
	}

	@Override
	public void resumePipeline() {
		((OutOrderExecutionEngine_amd)core.getExecEngine()).getFetcher().setSleep(false);
	}

	@Override
	public boolean isSleeping() {
		return ((OutOrderExecutionEngine_amd)core.getExecEngine()).getFetcher().isSleep();
	}

	@Override
	public void setTimingStatistics() {
		
		OutOrderExecutionEngine_amd execEngine = (OutOrderExecutionEngine_amd) core.getExecEngine();
		execEngine.getReorderBuffer().setTimingStatistics();
		
	}

	@Override
	public void setPerCoreMemorySystemStatistics() {
		
		OutOrderExecutionEngine_amd execEngine = (OutOrderExecutionEngine_amd) core.getExecEngine();
		execEngine.getReorderBuffer().setPerCoreMemorySystemStatistics();
		
	}


	@Override
	public void setExecutionComplete(boolean status) {
				
	}


	@Override
	public void adjustRunningThreads(int adjval) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setInputToPipeline(
			GenericCircularQueue<Instruction>[] inputToPipeline) {
		
		this.core.getExecEngine().setInputToPipeline(inputToPipeline);
		
	}

	@Override
	public long getBranchCount() {
		return ((OutOrderExecutionEngine_amd)core.getExecEngine()).getReorderBuffer().getBranchCount();
	}

	@Override
	public long getMispredCount() {
		return ((OutOrderExecutionEngine_amd)core.getExecEngine()).getReorderBuffer().getMispredCount();
	}

	@Override
	public long getNoOfMemRequests() {
		return ((OutOrderExecutionEngine_amd)core.getExecEngine()).getCoreMemorySystem().getLsqueue().noOfMemRequests;
	}

	@Override
	public long getNoOfLoads() {
		return ((OutOrderExecutionEngine_amd)core.getExecEngine()).getCoreMemorySystem().getLsqueue().NoOfLd;
	}

	@Override
	public long getNoOfStores() {
		return ((OutOrderExecutionEngine_amd)core.getExecEngine()).getCoreMemorySystem().getLsqueue().NoOfSt;
	}

	@Override
	public long getNoOfValueForwards() {
		return ((OutOrderExecutionEngine_amd)core.getExecEngine()).getCoreMemorySystem().getLsqueue().NoOfForwards;
	}

	//TODO split into iTLB and dTLB
	@Override
	public long getNoOfTLBRequests() {
		return ((OutOrderExecutionEngine_amd)core.getExecEngine()).getCoreMemorySystem().getiTLB().getTlbRequests()
				+ ((OutOrderExecutionEngine_amd)core.getExecEngine()).getCoreMemorySystem().getdTLB().getTlbRequests();
	}

	@Override
	public long getNoOfTLBHits() {
		return ((OutOrderExecutionEngine_amd)core.getExecEngine()).getCoreMemorySystem().getiTLB().getTlbHits()
				+ ((OutOrderExecutionEngine_amd)core.getExecEngine()).getCoreMemorySystem().getdTLB().getTlbHits();
	}

	@Override
	public long getNoOfTLBMisses() {
		return ((OutOrderExecutionEngine_amd)core.getExecEngine()).getCoreMemorySystem().getiTLB().getTlbMisses()
				+ ((OutOrderExecutionEngine_amd)core.getExecEngine()).getCoreMemorySystem().getdTLB().getTlbMisses();
	}

	@Override
	public long getNoOfL1Requests() {
		return ((OutOrderExecutionEngine_amd)core.getExecEngine()).getCoreMemorySystem().getL1Cache().noOfRequests;
	}

	@Override
	public long getNoOfL1Hits() {
		return ((OutOrderExecutionEngine_amd)core.getExecEngine()).getCoreMemorySystem().getL1Cache().hits;
	}

	@Override
	public long getNoOfL1Misses() {
		return ((OutOrderExecutionEngine_amd)core.getExecEngine()).getCoreMemorySystem().getL1Cache().misses;
	}

	@Override
	public long getNoOfIRequests() {
		return ((OutOrderExecutionEngine_amd)core.getExecEngine()).getCoreMemorySystem().getiCache().noOfRequests;
	}

	@Override
	public long getNoOfIHits() {
		return ((OutOrderExecutionEngine_amd)core.getExecEngine()).getCoreMemorySystem().getiCache().hits;
	}

	@Override
	public long getNoOfIMisses() {
		return ((OutOrderExecutionEngine_amd)core.getExecEngine()).getCoreMemorySystem().getiCache().misses;
	}
	

}
