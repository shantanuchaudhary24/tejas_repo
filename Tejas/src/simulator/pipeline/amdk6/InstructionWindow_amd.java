package pipeline.amdk6;

import java.io.FileWriter;
import java.io.IOException;

import pipeline.ExecutionEngine;

import config.PowerConfigNew;
import generic.Core;
import generic.Event;
import generic.EventQueue;
import generic.PortType;
import generic.SimulationElement;

public class InstructionWindow_amd extends SimulationElement {
	
	/*
	 * IW is implemented as an unordered buffer
	 * the precedence required when issuing instructions, is achieved by the ordering of the ROB
	 */
	
	Core core;
	IWEntry_amd[] IW;
	int maxIWSize;
	
	int[] availList;
	int availListHead;
	int availListTail;
	
	long numAccesses;
	
	public InstructionWindow_amd(Core core, OutOrderExecutionEngine_amd executionEngine)
	{
		super(PortType.Unlimited, -1, -1, core.getEventQueue(), -1, -1);
		this.core = core;
		
		maxIWSize = core.getIWSize();
		IW = new IWEntry_amd[maxIWSize];
		availList = new int[maxIWSize];
		for(int i = 0; i < maxIWSize; i++)
		{
			IW[i] = new IWEntry_amd(core, i, executionEngine, this);
			availList[i] = i;
		}
		availListHead = 0;
		availListTail = maxIWSize - 1;
		
	}
	
	public IWEntry_amd addToWindow(ReorderBufferEntry_amd ROBEntry)
	{
		int index = findInvalidEntry();
		if(index == -1)
		{
			//Instruction window full
			return null;
		}
		
		IWEntry_amd newEntry = IW[index];
		
		newEntry.setInstruction(ROBEntry.getInstruction());
		newEntry.setAssociatedROBEntry(ROBEntry);
		newEntry.setValid(true);
		
		ROBEntry.setAssociatedIWEntry(newEntry);
		
		incrementNumAccesses(1);
		
		return newEntry;
	}
		
	int findInvalidEntry()
	{
		if(availListHead == -1)
		{
			return -1;
		}
		
		int temp = availListHead;
		if(availListHead == availListTail)
		{
			availListHead = -1;
			availListTail = -1;
		}
		else
		{
			availListHead = (availListHead + 1)%maxIWSize;
		}
		
		return availList[temp];
	}
	
	public void removeFromWindow(IWEntry_amd entryToBeRemoved)
	{
		entryToBeRemoved.setValid(false);
		availListTail = (availListTail + 1)%maxIWSize;
		availList[availListTail] = entryToBeRemoved.pos;
		if(availListHead == -1)
		{
			availListHead = availListTail;
		}
		
		incrementNumAccesses(1);
	}
	
	public void flush()
	{
		for(int i = 0; i < maxIWSize; i++)
		{
			IW[i].setValid(false);
		}
	}

	public IWEntry_amd[] getIW() {
		return IW;
	}
	
	public boolean isFull()
	{
		if(availListHead == -1)
		{
			return true;
		}
		return false;
	}
	
	public int getMaxIWSize() {
		return maxIWSize;
	}

	@Override
	public void handleEvent(EventQueue eventQ, Event event) {
				
	}
	
	void incrementNumAccesses(int incrementBy)
	{
		numAccesses += incrementBy;
	}

	public PowerConfigNew calculateAndPrintPower(FileWriter outputFileWriter, String componentName) throws IOException
	{
		PowerConfigNew power = new PowerConfigNew(core.getIwPower(), numAccesses);
		power.printPowerStats(outputFileWriter, componentName);
		return power;
	}

}