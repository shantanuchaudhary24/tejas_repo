package config;
/* Defines the type of pipeline
 * */
	
public enum PipelineType {
	inOrder,outOfOrder,statistical,amdK6
}